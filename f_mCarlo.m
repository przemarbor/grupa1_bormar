function [wynik] = f_mCarlo(f, a, b, n)

%%%%%%%%%%% ARGUMENTY %%%%%%%%%%%%%
% f 	- funkcja podcalkowa w postaci uchwytu
% a,b 	- przedzial calkowania
% n     - ilosc probek wewnatrz <a,b>
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%<<<<<<<<<<<< ZWRACANE WARTOSCI <<
% I  - przyblizenie calki
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

if nargin < 4
    n = 1000 ;
end

% losowe argumenty w przedziale <a,b>
x_i = rand(1,n)*(b-a)+a ;
% wartosci funkcji dla powyzszych argumentach
f_x_i = f(x_i) ;
% wartosc maksymalna
maxf = max(f_x_i) ;
% losowe wsp. y z przedzialu <0, maxf> dla argumentow x_i
y_i = rand(1,n)*(maxf-0) ;
% liczba punktow ponizej wykresu funkcji
k = sum(y_i < f_x_i) ;

wynik = k/n * (b-a) * maxf ;

end % endfunction f_mCarlo
