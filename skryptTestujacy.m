clear all
close all
clc


%% Test 1
% wartosc 'dokladna'/referencyjna
val = quadl(@sin,0,pi,1000)
% wartosc obliczona metoda Monte Carlo
val2 = f_mCarlo(@sin,0,pi,1000)

%% Test 2
% granice calkowania
aa = 0 ;
bb = 3 ;
% liczba losowych probek
nn = 100 ;

fh = @(x) x.^2 - x ;

% wartosc 'dokladna'/referencyjna
val = quadl(fh,aa,bb,nn)

% wartosc obliczona metoda Monte Carlo
val2  = f_mCarlo(fh,aa,bb,nn)
% wywolanie funkcji Monte Carlo z 3 parametrami
val3  = f_mCarlo(fh,aa,bb)



